var detailListElem = document.getElementById('details-list')
var detailFormElem = document.getElementById('details-form')
var tableBody = document.getElementById('table-body')
let formElem = document.getElementById('form-elem')
let formTitle = document.getElementById('form-title')

var detailsList = [];
var detailId = ""
var formInputs = {
    firstName: document.getElementById('form-firstName'),
    lastName: document.getElementById('form-lastName'),
    dob: document.getElementById('form-dob'),
    gender: document.getElementById('form-gender'),
    hindi: document.getElementById('form-hindi'),
    maths: document.getElementById('form-maths'),
    science: document.getElementById('form-science'),
    isIndianYes: document.getElementById('form-isIndian-yes'),
    isIndianNo: document.getElementById('form-isIndian-no'),
}


let emptyTable = document.createElement('tr')
let td = document.createElement('td')
td.setAttribute('colspan', '9')
td.append("No data present")
emptyTable.append(td)

function toggleView(name, options) {
    if (name == 'form') {
        detailFormElem.classList.remove('hide')
        detailListElem.classList.add('hide')
        formElem.classList[options?.isView?'add':'remove']('view-only')
        console.log('toggle call setup', name, options?.isView)
        setupForm( options?.data, options?.isView )
    }
    else if (name == 'list') {
        detailFormElem.classList.add('hide')
        detailListElem.classList.remove('hide')
        setupList()
    } else {
        detailFormElem.classList.add('hide')
        detailListElem.classList.add('hide')
    }
}


function setupForm(editData, isView) {
    console.log('setupForm', isView)
    if (editData){
        formTitle.innerHTML = `${isView?'View':'Edit'} Form`
        detailId = editData.id
        formInputs.firstName.value = editData.firstName
        formInputs.lastName.value = editData.lastName
        formInputs.gender.value = editData.gender
        formInputs.dob.value = editData.dob
        formInputs.hindi.value = editData.hindi
        formInputs.maths.value = editData.maths
        formInputs.science.value = editData.science
        if (editData.isIndian=='Yes') formInputs.isIndianYes.checked = true
        else formInputs.isIndianNo.checked = true
    }else{
        console.log('New Form')
        formTitle.innerHTML = "Add Form"
        detailId = null
        formElem.reset()
    }
}

function deleteDetails(data) {
    console.log('to delete data', data)
    // i'm using local storage for now// this should be replace with a proper api calls
    let arrIndex = detailsList.findIndex( item => item.id == data.id )
    if ( arrIndex!= -1){
        detailsList.splice(arrIndex, 1)
        localStorage.setItem( 'listData',JSON.stringify( detailsList ) )
        alert('A form data has been deleted successfully!')
        toggleView('list')
    }

}

function validateAndSubmit(e) {
    e.preventDefault()
    // i'm using local storage for now// this should be replace with a proper api calls
    console.log('validateAndSubmit', formElem)
    let formData = new FormData(formElem)
    let jsonData = {}
    for (var pair of formData.entries()) {
        jsonData[pair[0]] = pair[1]
    }
    if (detailId){
        let arrIndex = detailsList.findIndex( item => item.id == detailId )
        if (arrIndex!=-1){
            detailsList[arrIndex] = { ...detailsList[arrIndex], ...jsonData }
            localStorage.setItem( 'listData',JSON.stringify( detailsList ) )
            alert('Form Date Updated successfully!')
        }
    }else{
        jsonData.id = new Date().getTime()
        detailsList.push(jsonData)
        localStorage.setItem( 'listData',JSON.stringify( detailsList ) )
        alert('Form Date Submitted successfully!')
    }
    formElem.reset()
    toggleView('form')

}

function setupList() {
    // i'm using local storage for now// this should be replace with a proper api calls
    detailsList = getList()
    tableBody.innerHTML = ""
    if (detailsList?.length) {

        for (let i = 0; i < detailsList.length; i++) {
            let tr = document.createElement('tr')
            let td0 = document.createElement('td')
            td0.append(detailsList[i].id)
            tr.append(td0)
            let td1 = document.createElement('td')
            td1.append(detailsList[i].firstName + ' ' + detailsList[i].lastName)
            tr.append(td1)
            let td2 = document.createElement('td')
            td2.append(detailsList[i].dob)
            tr.append(td2)
            let td3 = document.createElement('td')
            td3.append(detailsList[i].gender)
            tr.append(td3)
            let td4 = document.createElement('td')
            td4.append(detailsList[i].hindi)
            tr.append(td4)
            let td5 = document.createElement('td')
            td5.append(detailsList[i].maths)
            tr.append(td5)
            let td6 = document.createElement('td')
            td6.append(detailsList[i].science)
            tr.append(td6)
            let td7 = document.createElement('td')
            td7.append(detailsList[i].isIndian ? 'Yes' : 'No')
            tr.append(td7)
            let td8 = document.createElement('td')
            let editButton = document.createElement('button')
            editButton.append('Edit')
            editButton.addEventListener('click', (e) => { toggleView('form',  { data: detailsList[i], isView: false } ) })
            td8.append(editButton)
            let viewButton = document.createElement('button')
            viewButton.append('View')
            viewButton.addEventListener('click', (e) => { toggleView('form', { data: detailsList[i], isView: true } ) } )
            td8.append(viewButton)
            let deleteButton = document.createElement('button')
            deleteButton.append('Delete')
            deleteButton.addEventListener('click', (e) => { deleteDetails(detailsList[i]) })
            td8.append(deleteButton)
            tr.append(td8)

            tableBody.append(tr)
        }
    } else {
        let tr = document.createElement('tr')
        let td0 = document.createElement('td')
        td0.append("No data present")
        td0.setAttribute('colspan', '9')
        tr.append(td0)
        tableBody.append(tr)
    }
}
function getList() {
    // i'm using local storage for now// this should be replace with a proper api calls
    let listData = localStorage.getItem('listData')
    listData = listData ? JSON.parse(listData) : []
    return listData
}